# Aritmética modular
[https://es.khanacademy.org/computing/computer-science/cryptography](https://es.khanacademy.org/computing/computer-science/cryptography) 

## Algoritmo de Euclides para calcular el m.c.d de dos números

Recuerda que el máximo común divisor (MCD) de dos enteros A y B es el entero más grande que divide tanto a A como a B.

El algoritmo de Euclides es una técnica para encontrar rápidamente el MCD de dos enteros.

### El algoritmo de Euclides 

Para encontrar MCD(A,B) es como sigue:    

* Si A = 0 entonces MCD(A,B)=B, ya que el MCD(0,B)=B, y podemos detenernos.  
* Si B = 0 entonces MCD(A,B)=A, ya que el MCD(A,0)=A, y podemos detenernos.  
* Escribe A en la forma cociente y residuo (A = B * Q + R).
* Encuentra MCD(B,R) al usar el algoritmo de Euclides, ya que MCD(A,B) = MCD(B,R).

**Ejemplo:**

Encuentra el MCD de 270 y 192.

A=270, B=192.

Como $A \neq 0 \land B \neq 0$, hacemos la división y obtenemos que $270 = 192 \cdot 1 + 78$
Encuentra MCD(192,78), ya que MCD(270,192)=MCD(192,78).

A=192, B=78.

    A <> 0
    B <> 0
    Usa división larga para encontrar que 192/78 = 2 con un residuo de 36. Podemos escribir esto como:
    192 = 78 * 2 + 36
    Encuentra MCD(78,36), ya que MCD(192,78)=MCD(78,36).

A=78, B=36.

    A <> 0
    B <> 0
    Usa división larga para encontrar que 78/36 = 2 con un residuo de 6. Podemos escribir esto como:
    78 = 36 * 2 + 6
    Encuentra MCD(36,6), ya que MCD(78,36)=MCD(36,6).

A=36, B=6.

    A <> 0
    B <> 0
    Usa división larga para encontrar que 36/6 = 6 con un residuo de 0. Podemos escribir esto como:
    36 = 6 * 6 + 0
    Encuentra MCD(6,0), ya que MCD(36,6)=MCD(6,0).

A=6, B=0.

    A <> 0
    B <> 0, MCD(6,0)=6.

Así que hemos mostrado:
MCD(270,192) = MCD(192,78) = MCD(78,36) = MCD(36,6) = MCD(6,0) = 6.
MCD(270,192) = 6

**Código que implementa el algoritmo**

```python
def euclides(a, b) :
    if b > a :
        return euclides(b,a)
    elif  b == 0 :
        return a
    else :
        r = a%b
        return b if r == 0 else euclides(b,r)
```

## Identidad de Bezout

[https://www.youtube.com/watch?v=jqWhjrjLKxY](https://www.youtube.com/watch?v=jqWhjrjLKxY)

**Ejemplo:**

Calcular los coeficientes de Bezout para los números 141 y 96:

*Solución:*

Siguiendo el algoritmo de Euclides tenemos que:

$$141=1\cdot 96 + 45 \to 45=1\cdot 141-1\cdot96 \to 45=(1-1\cdot 0)\cdot 141+(0-1\cdot 1)\cdot96$$
$$96=2\cdot 45 + 6 \to 6=96 - 2\cdot 45=(0-(2\cdot 1))\cdot 141 + (1 -2\cdot (-1))\cdot 96=-2\cdot 141 + 3\cdot 96  $$
$$45=7\cdot 6+3$$
$$6=2\cdot 3 + 0$$

Podemos poner la información en una tabla teniendo en cuenta que:

$u_n=u_{n-2}-c_n \cdot u_{n-1}$

|   r |   c |   u |   v |
|----:|----:|----:|----:|
| 141 |   0 |   1 |   0 |
|  96 |   0 |   0 |   1 |
|  45 |   1 |   1 |  -1 |
|   6 |   2 |  -2 |   3 |
|   3 |   7 |  15 | -22 |

## Ecuaciones diofánticas lineales de una y dos variables
[http://joseluislorente.es/academia/temas/Tema%2015.pdf](http://joseluislorente.es/academia/temas/Tema%2015.pdf)


Una ecuación diofántica lineal es toda aquella de la forma: $$a_1\cdot x_1+a_2\cdot x_2+\cdots+a_n \cdot x_n=c$$ siendo $a_i$,$c$, $x_i \in \mathbb{Z}$. Si $c=0$ la ecuación diofántica se dice que es homogénea.

## Módulo m

## Sistemas de congruencias lineales 
## Unidades y divisores en Z/mZ

## Pequeño teorema de Fermat para ver si un entero es primo

## Algoritmos de cifrado de sustitución, polialfabéticos y RSA

## Algoritmo de Euclides para el cálculo del MCD

* Explicación

Es bastante sencillo, calculamos el resto de la división entre a y b (dividendo y divisor, en este caso). Si el resto es distinto de 0, el mcd(a,b)=mcd(b,r) y llamamos recurrentemente a la función para calcular mcd(b,r). El proceso se repetirá hasta que llegemos a que mcd(a,b)=mcd(x,0)=x

* Primera versión

Lo anterior se implementa fácilmente con el siguiente código:


```python
def euclides(a, b) :
    r = a%b
    return b if r == 0 else euclides(b,r)
```


```python
euclides(12,16)
```




    4



* Segunda versión

La versión anterior solo funciona si $a > b$ y $a \neq 0 \land b \neq 0$. Podemos modificar el código para que controle esos aspectos:


```python
def euclides(a, b) :
    if b > a :
        return euclides(b,a)
    elif b == 0 :
        return a
    else :
        r = a%b
        return b if r == 0 else euclides(b,r)
```


```python
euclides(12,0)
```




    12
* Tercera versión

Podemos modificar el código para que vaya explicando todos los pasos que vaya haciendo:


```python
# from IPython.display import display, Markdown, Latex
# print("mcd({},{})={}".format(b,r,b))

def euclides(a, b) :
    print("mcd({},{})=".format(a,b))
    if b > a :
        return euclides(b,a)
    elif b == 0 :
        print("{}".format(a))
        return a
    else :
        r = a%b
        return b if r == 0 else euclides(b,r)
        
#         if r == 0 :
#             print("acabo")
#             return b 
#         else :
#             euclides(b,r)
```


```python
euclides(12,16)
```

    mcd(12,16)=
    mcd(16,12)=
    mcd(12,4)=





    4

