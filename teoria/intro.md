# Introducción

La presente documentación tiene el objetivo de servir de guía para la impartición de la asignatura Matemáticas para la toma de decisiones ofertada como optativa en 4º de la ESO en la comunidad de Aragón.

Para el primer tema, que básicamente se enfoca en la criptografía, podemos seguir el siguiente libro: [https://www.unirioja.es/cu/jvarona/downloads/teoria_numeros_2aed_android.pdf](https://www.unirioja.es/cu/jvarona/downloads/teoria_numeros_2aed_android.pdf)

Algoritmo extendido de euclides explicado: [http://esfm.egormaximenko.com/linalg/gcd_extended_es.pdf](http://esfm.egormaximenko.com/linalg/gcd_extended_es.pdf)

Para teoría de grafos: [https://posgrados.inaoep.mx/archivos/PosCsComputacionales/Curso_Propedeutico/Matematicas_Discretas/Capitulo_4_Grafos.pdf](https://posgrados.inaoep.mx/archivos/PosCsComputacionales/Curso_Propedeutico/Matematicas_Discretas/Capitulo_4_Grafos.pdf)
[https://ptwiddle.github.io/Graph-Theory-Notes/s_intro_instantinsanity.html](https://ptwiddle.github.io/Graph-Theory-Notes/s_intro_instantinsanity.html)

